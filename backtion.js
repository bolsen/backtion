var Backtion = (function() {

  // These functions are pulled from Backbone.

  var ctor = function() {};

  var inherits = function(parent, protoProps, staticProps) {
    var child;

    if (protoProps && protoProps.hasOwnProperty('constructor')) {
      child = protoProps.constructor;
    } else {
      child = function(){ parent.apply(this, arguments); };
    }

    _.extend(child, parent);
    ctor.prototype = parent.prototype;
    child.prototype = new ctor();

    if (protoProps) _.extend(child.prototype, protoProps);
    if (staticProps) _.extend(child, staticProps);

    child.prototype.constructor = child;

    child.__super__ = parent.prototype;

    return child;
  };

  var extend = function (protoProps, classProps) {
    var child = inherits(this, protoProps, classProps);
    child.extend = this.extend;
    return child;
  };

  ////


  function shouldBeFunc(func, alternateFunc) {
    if(typeof func === 'function')
      return func;
    else
      return (alternateFunc ? alternateFunc : function() {});
  }

  var Backtion = function(attrs) {
    this.attributes = attrs || {};
  };

  _.extend(Backtion.prototype, {

    url        : function()                  { return this.urlRoot; },
    method     : function()                  { return this.baseMethod; },
    validate   : function(attrs)             { return true; },
    parse      : function(resp, status, xhr) { return {}; },
    toJSON     : function()                  { return this.attributes; },
    errors     : [],

    set: function(params, options) {
      params = params || {};
      options = options || {};
      var newAttrs = _.extend(this.attributes, params);
      var errors   = this.errors = this.validate(newAttrs);

      if(_.isEmpty(errors)) {
        this.attributes = newAttrs;
        return true;
      } else {
        shouldBeFunc(options.error)(this, errors);
        return false;
      }
    },

    run: function(options) {
      options = options || {};
      var model        = this;
      var userSuccess  = shouldBeFunc(options.success);
      var userError = shouldBeFunc(options.error);

      options.success = function(resp, status, xhr) {
        var parsedResult = model.parse(JSON.parse(xhr.responseText));
        if(options.validate) {
          if(!model.validate(parsedResult)) {
            return false;
          }
        }

        if(typeof model.success === 'function')
          model.success(parsedResult);

        return userSuccess(model, resp, xhr);
      };

      options.error = function(xhr, state, textStatus) {
        if(xhr.getResponseHeader("Content-Type").match(/json/)) {
          var data = JSON.parse(xhr.responseText);
        } else {
          var data = xhr.responseText;
        }

        if(typeof model.error === 'function') {
          model.error(data, xhr, state, textStatus);
        }

        userError(model, data, xhr, state, textStatus);
      };

      return (model.impl || Backtion.impl)(model, options);
    }

  });



  // AJAX/jQuery implementation of executing

  Backtion.impl = function(model, options) {
    var done    = options.done; delete options.done;
    var success = options.success; delete options.success;
    var fail    = options.fail; delete options.fail;
    var error   = options.error; delete options.error;
    var always  = options.always; delete options.always;
    var then    = options.then; delete options.then;

    $.ajax(_.extend({url: model.url(), type: model.method(), data: model.toJSON(), dataType: 'json'}, options)).
      done(done || success).
      fail(fail || error).
      always(always).
      then(then);
  };


  Backtion.extend = extend;

  return Backtion;
})();
